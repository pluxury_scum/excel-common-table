import numpy
import pandas
from sklearn.metrics import r2_score


class ML:
    """Working with data for machine learning purposes"""

    def main(self):
        """Program entry point"""

        numFiles = 10149  # amount of files to research
        x_param = input("Input the parameter X you want to research: ")
        y_param = input("Input the parameter y you want to research: ")
        train_perc = input("Input the amount of data to train on (in %): ")
        train_amount = int(numFiles) * int(train_perc) / 100
        test_perc = input("Input the amount of data to test on (in %): ")
        test_amount = int(numFiles) * int(test_perc) / 100
        polyfit_deg = input("Input the degree of the fitting polynomial: ")
        research_val = input("Input the researching value for the model: ")

        df = pandas.read_excel("TotalWB.xlsx")  # read excel file

        X = df[x_param]
        y = df[y_param]

        train_x = X[:int(train_amount)]  # train data X
        train_y = y[:int(train_amount)]  # train data y

        test_x = X[int(test_amount):]  # test data X
        test_y = y[int(test_amount):]  # test data y

        mymodel = numpy.poly1d(numpy.polyfit(train_x, train_y, int(polyfit_deg)))  # create polynominal regression model

        # Check if there is good dependency.
        # r2_train = r2_score(train_y, mymodel(train_x))
        # r2_test = r2_score(test_y, mymodel(test_x))

        print(mymodel(int(research_val)))